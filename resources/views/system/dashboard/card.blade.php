<div class="col-lg-3 col-md-6 col-sm-6 col-12">
    <div class="card card-statistic-1 card-primary">
        <i class="fas fa-users card-icon col-red"></i>
        <div class="card-wrap">
            <div class="padding-20">
                <div class="text-right">
                    <h3 class="font-light mb-0">
                        <i class="ti-arrow-up text-success"></i> {{$vendor}}
                    </h3>
                    <span class="text-muted">Total Vendor</span>
                </div>
                <p class="mb-3 text-muted pull-left text-sm">
                    <!-- <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 10%</span>
                    <span class="text-nowrap">Since last month</span> -->
                </p>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 col-sm-6 col-12">
    <div class="card card-statistic-1 card-primary">
        <i class="fas fa-users card-icon col-green"></i>
        <div class="card-wrap">
            <div class="padding-20">
                <div class="text-right">
                    <h3 class="font-light mb-0">
                        <i class="ti-arrow-up text-success"></i> {{$seller}}
                    </h3>
                    <span class="text-muted">Total Seller</span>
                </div>
                <p class="mb-3 text-muted pull-left text-sm">
                    <!-- <span class="col-orange mr-2"><i class="fa fa-arrow-down"></i> 5%</span>
                    <span class="text-nowrap">Since last month</span> -->
                </p>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 col-sm-6 col-12">
    <div class="card card-statistic-1 card-primary">
        <i class="fas fa-users card-icon col-orange"></i>
        <div class="card-wrap">
            <div class="padding-20">
                <div class="text-right">
                    <h3 class="font-light mb-0">
                        <i class="ti-arrow-up text-success"></i> {{$reseller}}
                    </h3>
                    <span class="text-muted">Total Reseller</span>
                </div>
                <p class="mb-3 text-muted pull-left text-sm">
                    <!-- <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 24%</span>
                    <span class="text-nowrap">Since last month</span> -->
                </p>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-3 col-md-6 col-sm-6 col-12">
    <div class="card card-statistic-1 card-primary">
        <i class="fas fa-users card-icon col-cyan"></i>
        <div class="card-wrap">
            <div class="padding-20">
                <div class="text-right">
                    <h3 class="font-light mb-0">
                        <i class="ti-arrow-up text-success"></i> {{$user}}
                    </h3>
                    <span class="text-muted">Total User</span>
                </div>
                <p class="mb-3 text-muted pull-left text-sm">
                    <!-- <span class="col-orange mr-2"><i class="fa fa-arrow-down"></i> 7.5%</span>
                    <span class="text-nowrap">Since last month</span> -->
                </p>
            </div>
        </div>
    </div>
</div>