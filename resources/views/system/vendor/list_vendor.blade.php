@extends('layouts.index')

@section('content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="#" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>List Vendor</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Beranda</a></div>
            <div class="breadcrumb-item">List Vendor Aktif</div>
        </div>
    </div>
    <div class="section-body">
        <a href="{{ route('vendor.register') }}" class="btn btn-primary btn-sm">Tambah Vendor</a>
        <hr>
        <div class="row">
            @foreach($user as $us)
            <div class="col-12 col-md-4 col-lg-4">
                <article class="article article-style-c">
                    <div class="article-details">
                        <div class="article-category"><a href="#">Mendaftar</a>
                            <div class="bullet"></div> <a href="#">{{ !empty($us->user) ? $us->user->created_at : '' }}</a>
                        </div>
                        <div class="article-title float-left">
                            <h2><a href="#">{{ !empty($us->user) ? $us->user->name : '' }}</a>
                            </h2>
                        </div>
                        <div class="article-title float-right">
                            <!-- <h2>
                                {!! $us->nm_role !!}
                            </h2> -->
                        </div>
                        <!-- <p>Ini adalah Deskripsi</p> -->
                        <div class="article-user">
                            @if ($us->foto_user == null)
                            <img alt="image" src="{{ asset('assets/img/users/user-1.png') }}">
                            @else
                            <img alt="image" src="{{Storage::disk('s3')->url('public/images/profile/' . $us->foto_user)}}">
                            @endif
                            <div class="article-user-details">
                                <div class="user-detail-name">
                                    <a href="#">{{ !empty($us->user) ? $us->user->email : '' }}</a>
                                </div>
                                <div class="text-job">{{ !empty($us->role) ? $us->role->nm_role : '' }}</div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            @endforeach
        </div>
        {{ $user->links() }}
    </div>
</section>
@endsection