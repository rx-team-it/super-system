@extends('layouts.index')

@section('content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('vendor.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>List Vendor</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Beranda</a></div>
            <div class="breadcrumb-item active"><a href="{{ route('vendor.index') }}">List Vendor Aktif</a></div>
            <div class="breadcrumb-item active">Pendaftaran Vendor</div>
        </div>
    </div>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">
                <div class="card card-primary">
                    <div class="card-header">
                        <h4>Form Pendaftaran Vendor</h4>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('vendor.post') }}" id="regist_form" name="regist_form">
                            @csrf
                            <div class="row">
                                <div class="form-group col-6">
                                    <label>Nama</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Nama Lengkap">
                                    <span class="text-danger error-text name_error"></span>
                                </div>
                                <div class="form-group col-6">
                                    <label>No Hp</label>
                                    <input type="number" class="form-control" id="phone" name="phone" placeholder="Nomor Hp">
                                    <span class="text-danger error-text phone_error"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                                <span class="text-danger error-text email_error"></span>
                            </div>
                            <div class="row">
                                <div class="form-group col-6">
                                    <label class="d-block">Password</label>
                                    <input type="password" class="form-control" name="password" placeholder="Password">
                                    <span class="text-danger error-text password_error"></span>
                                </div>
                                <div class="form-group col-6">
                                    <label class="d-block">Konfirmasi Password</label>
                                    <input type="password" class="form-control" name="password_confirmation" placeholder="Konfirmasi Password">
                                    <span class="text-danger error-text password_error"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Url Web</label>
                                <input type="text" class="form-control" name="url_site" placeholder="url web">
                                <span class="text-danger error-text url_site_error"></span>
                            </div>
                            <div class="row">
                                <div class="form-group col-6">
                                    <label class="d-block">Domain</label>
                                    <input type="text" class="form-control" name="domain" placeholder="Domain">
                                    <span class="text-danger error-text domain_error"></span>
                                </div>
                                <div class="form-group col-6">
                                    <label class="d-block">Nama Web</label>
                                    <input type="text" class="form-control" name="name_site" placeholder="Nama Web">
                                    <span class=" text-danger error-text name_site_error"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-4">
                                    <label class="d-block">Handphone</label>
                                    <input type="text" class="form-control" name="contact_phone" placeholder="No Handphone">
                                    <span class="text-danger error-text contact_phone_error"></span>
                                </div>
                                <div class="form-group col-4">
                                    <label class="d-block">Alamat</label>
                                    <input type="text" class="form-control" name="contact_address" placeholder="Alamat">
                                    <span class=" text-danger error-text contact_address_error"></span>
                                </div>
                                <div class="form-group col-4">
                                    <label class="d-block">Email</label>
                                    <input type="text" class="form-control" name="contact_email" placeholder="Email Yang Bisa Dihubungi">
                                    <span class=" text-danger error-text contact_email_error"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Deksripsi Web</label>
                                <input type="text" class="form-control" name="description" placeholder="Deksripsi Web">
                                <span class="text-danger error-text description_error"></span>
                            </div>
                            <div class="form-group">
                                <label>Meta Text</label>
                                <input type="text" class="form-control" name="meta_desc" placeholder="Meta Text">
                                <span class="text-danger error-text meta_desc_error"></span>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block">
                                    Buat Vendor
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script>
    $(function() {
        $("#regist_form").on('submit', function(e) {
            e.preventDefault();

            $.ajax({
                url: $(this).attr('action'),
                method: $(this).attr('method'),
                data: new FormData(this),
                processData: false,
                dataType: 'json',
                contentType: false,
                beforeSend: function() {
                    $(document).find('span.error-text').text('');
                },
                success: function(data) {
                    if (data.status == 0) {
                        $.each(data.error, function(prefix, val) {
                            $('span.' + prefix + '_error').text(val[0]);
                        });
                    } else {
                        document.regist_form.reset();
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000,
                            timerProgressBar: true,
                            didOpen: (toast) => {
                                toast.addEventListener('mouseenter', Swal.stopTimer)
                                toast.addEventListener('mouseleave', Swal.resumeTimer)
                            }
                        })

                        Toast.fire({
                            icon: 'success',
                            title: 'Vendor Berhasil Dibuat'
                        })
                    }
                }
            })
        });
    });
</script>

@endsection