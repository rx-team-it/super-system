@extends('layouts.index')

@section('css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('vendor.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Beranda</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('vendor.index') }}">Beranda</a></div>
            <div class="breadcrumb-item">Daftar Riwayat Gambar</div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Daftar Riwayat Gambar</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="tbl" class="table table-hover table-bordered table-sm">
                            <thead>
                                <th>No</th>
                                <th>Gambar</th>
                                <th>Diubah</th>
                            </thead>
                            <tbody>
                                @foreach ($w as $item)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td><img src="{{Storage::disk('s3')->url('public/images/product/'. $item->gambar)}}" height="80px" width="80px"> <br> <small>Ukuran :&nbsp; {{ $item->ukuran }}
                                            MB</small></td>
                                    <td>
                                        {{$item->created_at}} oleh: {{$item->name}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Grafik Total Ukuran Gambar Terpakai</h4>
                </div>
                <div class="card-body">
                    <div id="echart_donut" class="chartsh chart-shadow2"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- JS Libraies -->
<script src="{{asset('assets/bundles/echart/echarts.js')}}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#tbl').DataTable({
            "pageLength": 5,
            "lengthMenu": [5, 20, 50, 75, 100]
        });
    });
</script>
<script type="text/javascript">
    // based on prepared DOM, initialize echarts instance
    var myChart = echarts.init(document.getElementById('echart_donut'));
    var e = <?php echo json_encode($e->size); ?>;
    // specify chart configuration item and data
    var option = {
        tooltip: {
            trigger: 'item'
        },
        legend: {
            top: '5%',
            left: 'center'
        },
        series: [{
            name: 'Total Pemakaian',
            type: 'pie',
            radius: ['40%', '70%'],
            avoidLabelOverlap: false,
            label: {
                show: false,
                position: 'center'
            },
            emphasis: {
                label: {
                    show: true,
                    fontSize: '40',
                    fontWeight: 'bold'
                }
            },
            labelLine: {
                show: false
            },
            data: [{
                value: e,
                name: 'Mb'
            }]
        }]
    };

    // use configuration item and data specified to show chart
    myChart.setOption(option);
</script>
@endsection