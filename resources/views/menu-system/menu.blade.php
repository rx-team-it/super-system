<li class="menu-header">PANEL VENDOR</li>
<li class="dropdown">
    <a href="#" class="nav-link has-dropdown"><i class="fas fa-qrcode"></i><span>Management Vendor</span></a>
    @include('menu-system.partials.menu-vendor')
</li>

<li class="dropdown">
    <a href="#" class="nav-link has-dropdown"><i class="fas fa-file-image"></i><span>Storage Aws</span></a>
    @include('menu-system.partials.menu-storage')
</li>