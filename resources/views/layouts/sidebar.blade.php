<aside id="sidebar-wrapper">
    <div class="sidebar-brand">
        <a href="{{ route('dashboard') }}">
            <img alt="image" src="{{ Storage::disk('s3')->url('public/images/ababill-2-trans22.ico') }}" class="header-logo" />
            <span class="logo-name">ABBABILL</span>
        </a>
    </div>
    <ul class="sidebar-menu">
        <li class="menu-header">Main</li>
        <li class="dropdown">
            <a href="{{ route('dashboard') }}" class="nav-link">
                <i class="fas fa-home"></i><span>Beranda</span>
            </a>
        </li>
        @include('menu-system.menu')
    </ul>
</aside>