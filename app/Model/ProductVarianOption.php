<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductVarianOption extends Model
{
    use SoftDeletes;

    protected $table = 'product_varian_option';
    protected $primaryKey = 'id';
    protected $guarded = [''];

    public function product_varian_option()
    {
        return $this->belongsTo('App\Model\ProductVarian', 'product_varian_id');
    }

    public function product_image_option()
    {
        return $this->hasMany('App\Model\ProductImage', 'product_varian_option_id');
    }

    public function product_detail()
    {
        return $this->hasMany('App\Model\ProductDetail');
    }

    public function varian()
    {
        return $this->belongsTo('App\Model\ProductVarian', 'product_varian_id');
    }

    public function parent_option()
    {
        return $this->belongsTo(ProductVarianOption::class);
    }

    public function child()
    {
        return $this->hasMany(ProductVarianOption::class, 'parent_option');
    }
}
