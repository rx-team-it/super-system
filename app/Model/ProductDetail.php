<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model
{
    protected $table = 'product_detail';
    protected $primaryKey = 'id';
    protected $guarded = [''];

    public function product_varian_option()
    {
        return $this->belongsTo('App\Model\ProductVarianOption', 'product_varian_id');
    }

    public function varian_option()
    {
        return $this->belongsTo('App\Model\ProductVarianOption', 'product_varian_option_id');
    }
}
