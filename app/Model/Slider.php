<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $table = 'image_landing';
    protected $primaryKey = 'id';
    protected $guarded = [''];

}
