<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MasterBank extends Model
{
    protected $table = 'master_bank';
    protected $primaryKey = 'id';
    protected $guarded = [''];
}
