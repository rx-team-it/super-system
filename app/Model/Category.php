<?php

namespace App\Model;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use App\Model\MasterStore;

class Category extends Model
{
	protected $table = 'category';
	protected $primaryKey = 'id';
	protected $guarded = [''];

	public function parent()
	{
		return $this->belongsTo('App\Model\Category', 'parent_category');
	}

	public function scopeGetParent($query)
	{
		return $query->whereNull('parent_category');
	}

	public function scopeGetIndex($query)
	{
		return $query->whereNotNull('parent_category');
	}

	public function setSlugAttribute($value)
	{
		$this->attributes['slug'] = Str::slug($value);
	}

	public function getNameAttribute($value)
	{
		return ucfirst($value);
	}

	public function child()
	{
		return $this->hasMany('App\Model\Category', 'parent_category');
	}

	public function product()
	{
		return $this->hasMany('App\Model\Product');
	}

	public function getCreatedAtAttribute()
	{
		return \Carbon\Carbon::parse($this->attributes['created_at'])
			->format('d M Y ');
	}

	// public function scopeCategory($query)
	// {
	//     return $query->whereNull('parent_category');
	// }

	// public function scopeSubCategory($query)
	// {
	//     return $query->whereNotNull('parent_category');
	// }

	// public function product()
	// {
	//     return $this->hasMany(Product::class);
	// }
}
