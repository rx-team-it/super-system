<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserDetails extends Model
{
    use SoftDeletes;

    protected $table = 'user_details';
    protected $primaryKey = 'id';
    protected $guarded = [''];

    public function getImageAttribute($value)
    {
        return env('APP_URL') . "/storage/profile/" . $value;
    }

    public function countrys()
    {
        return $this->belongsTo('App\Model\Country', 'province');
    }

    public function citys()
    {
        return $this->belongsTo('App\Model\City', 'district');
    }

    public function states()
    {
        return $this->belongsTo('App\Model\State', 'city');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'id_user');
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }
}
