<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $table = 'user_profile';
    protected $guarded = [''];

    public function countrys()
    {
        return $this->belongsTo('App\Model\Country', 'province');
    }

    public function citys()
    {
        return $this->belongsTo('App\Model\City', 'district');
    }

    public function states()
    {
        return $this->belongsTo('App\Model\State', 'city');
    }
}
