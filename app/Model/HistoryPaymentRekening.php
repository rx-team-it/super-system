<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class HistoryPaymentRekening extends Model
{
    protected $table = 'history_payment_rekening';
    protected $primaryKey = 'id';
    protected $guarded = [''];
    protected $appends = ['status_label'];

    public function getStatusLabelAttribute()
    {
        if ($this->status == 0) {
            return '<button type="button" class="btn btn-primary btn-sm btn-icon icon-left">
            <i class="fas fa-exclamation-circle"></i> Sedang Dalam Pengecekan
            </button>';
        } elseif ($this->status == 1) {
            return '<button type="button" class="btn btn-success btn-sm btn-icon icon-left">
            <i class="fas fa-check"></i> Penarikan Berhasil
            </button>';
        } elseif ($this->status == 2) {
            return '<button type="button" class="btn btn-danger btn-sm btn-icon icon-left">
            <i class="fas fa-exclamation-circle"></i> Penarikan Gagal
            </button>';
        }
        return 'Selesai';
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function user()
    {
        return $this->belongsTo('App\Model\UserProfile', 'user_id');
    }

    public function store()
    {
        return $this->belongsTo('App\Model\MasterStore', 'store_id');
    }

    public function payment()
    {
        return $this->belongsTo('App\Model\HistoryPayment', 'history_payment_id');
    }
}
