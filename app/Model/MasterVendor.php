<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MasterVendor extends Model
{
    protected $table = 'master_vendor';
    protected $primaryKey = 'id';
    protected $guarded = [''];
}
