<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';
    protected $primarykey = 'id';
    protected $guarded = [''];
    protected $appends = ['status_label'];
    public $timestamps = false;

    public function product()
    {
        return $this->belongsTo('App\Model\Product', 'product_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Model\Country');
    }

    public function address()
    {
        return $this->belongsTo('App\Model\MasterAdress');
    }

    public function detail()
    {
        return $this->belongsTo('App\Model\OrderDetail');
    }

    public function test_order_details()
    {
        return $this->hasMany('App\Model\OrderDetail', 'order_id');
    }

    public function finance()
    {
        return $this->hasMany('App\Model\Finance', 'order_id');
    }

    public function payment()
    {
        return $this->belongsTo('App\Model\ConfirmPayment', 'user_id');
    }

    public function getStatusLabelAttribute()
    {
        if ($this->status == 0) {
            return '<button type="button" class="btn btn-danger btn-sm btn-icon icon-left">
            <i class="fas fa-exclamation-circle"></i> Belum Bayar
            </button>';
        } elseif ($this->status == 1) {
            return '<button type="button" class="btn btn-primary btn-sm btn-icon icon-left">
            <i class="fas fa-check"></i> Sudah Bayar
            </button>';
        } elseif ($this->status == 2) {
            return '<button type="button" class="btn btn-danger btn-sm btn-icon icon-left">
            <i class="fas fa-exclamation-circle"></i> Pembayaran Gagal
            </button>';
        } elseif ($this->status == 3) {
            return '<button type="button" class="btn btn-info btn-sm btn-icon icon-left">
            <i class="fas fa-box"></i> Dikemas
            </button>';
        } elseif ($this->status == 4) {
            return '<button type="button" class="btn btn-info btn-sm btn-icon icon-left">
            <i class="fas fa-plane-departure"></i> Dikirim
            </button>';
        } elseif ($this->status == 5) {
            return '<button type="button" class="btn btn-success btn-sm btn-icon icon-left">
            <i class="fas fa-check"></i> Selesai
            </button>';
        }
        return 'Selesai';
        // if ($this->status == 0) {
        //     return '<span class="badge badge-secondary">Baru</span>';
        // } elseif ($this->status == 1) {
        //     return '<span class="badge badge-primary">Konfirmasi</span>';
        // } elseif ($this->status == 2) {
        //     return '<span class="badge badge-info">Proses</span>';
        // } elseif ($this->status == 3) {
        //     return '<span class="badge badge-warning">Dikirim</span>';
        // } elseif ($this->status == 4) {
        //     return '<span class="badge badge-danger">Cancel</span>';
        // }
        // return '<span class="badge badge-success">Selesai</span>';
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function order_details()
    {
        return $this->hasMany('App\Model\OrderDetail', 'order_id');
    }

    public function store()
    {
        return $this->belongsTo('App\Model\MasterStore', 'store_id');
    }
}
