<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class MasterCategory extends Model
{
    protected $table = 'master_category';
    protected $primarykey = 'id';
    protected $guarded = [''];

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }
}
