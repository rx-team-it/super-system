<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HistoryPayment extends Model
{
    protected $table = 'history_payment';
    protected $primaryKey = 'id';
    protected $guarded = [''];
}
