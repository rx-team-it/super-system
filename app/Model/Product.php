<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\ProductVarian;

class Product extends Model
{
    use SoftDeletes;

    protected $table = 'product';
    protected $primaryKey = 'id';
    protected $guarded = [''];

    public function scopeGetParent($query)
    {
        return $query->whereNull('parent_category');
    }

    public function getStatusLabelAttribute()
    {
        if ($this->status == 0) {
            return '<span class="badge badge-secondary">Proses</span>';
        } elseif ($this->status == 1) {
            return '<span class="badge badge-primary">Approve</span>';
        } elseif ($this->status == 2) {
            return '<span class="badge badge-warning">Blok</span>';
        } elseif ($this->status == 3) {
            return '<span class="badge badge-danger">Delete</span>';
        }
        return '<span class="badge badge-success">Selesai</span>';
    }

    public function category()
    {
        return $this->belongsTo('App\Model\Category', 'id_category');
    }

    public function order()
    {
        return $this->hasMany('App\Model\Order', 'product_id');
    }

    public function product_varians()
    {
        return $this->hasMany('App\Model\ProductVarian', 'product_id');
    }

    public function product_images_thumbnail()
    {
        return $this->hasOne('App\Model\ProductImage', 'product_id');
    }

    public function product_images()
    {
        return $this->hasMany('App\Model\ProductImage', 'product_id');
    }

    public function order_detail()
    {
        return $this->hasMany('App\Model\OrderDetail', 'product_id');
    }
    public function product_detail()
    {
        return $this->hasOne('App\Model\ProductDetail', 'product_id');
    }


    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }
}
