<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Finance extends Model
{
    protected $table = 'transaction';
    protected $primaryKey = 'id';
    protected $guarded = [''];

    public function order()
    {
        return $this->belongsTo('App\Model\Order', 'order_id');
    }

    public function orderdetail()
    {
        return $this->belongsTo('App\Model\OrderDetail', 'detail_id');
    }

    public function store()
    {
        return $this->belongsTo('App\Model\MasterStore', 'store_id');
    }
}
