<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductVendor extends Model
{
    protected $table = 'product_vendor';
    protected $primaryKey = 'id';
    protected $guarded = [''];

    public function product()
    {
        return $this->belongsTo('App\Model\Product');
    }
}
