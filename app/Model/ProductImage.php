<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductImage extends Model
{
    use SoftDeletes;

    protected $table = 'product_image';
    protected $primaryKey = 'id';
    protected $guarded = [''];

    public function product()
    {
        return $this->belongsTo('App\Model\Product', 'product_id');
    }

    public function product_varian_option()
    {
        return $this->belongsTo('App\Model\ProductVarianOption', 'product_varian_option_id');
    }
}
