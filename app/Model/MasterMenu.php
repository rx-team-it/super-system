<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MasterMenu extends Model
{
    protected $table = 'master_menu';
    protected $primaryKey = 'id';
    protected $guarded = [''];

    public function menu()
    {
        return $this->belongsTo('App\Model\MasterContent');
    }
}
