<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';
    protected $primaryKey = 'id';
    protected $guarded = [''];

    public function state()
    {
        return $this->hasMany(State::class);
    }

    public function address()
    {
        return $this->hasMany('App\Model\MasterAdress');
    }
}
