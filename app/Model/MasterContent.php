<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MasterContent extends Model
{
    protected $table = 'master_content_menu';
    protected $primaryKey = 'id';
    protected $guarded = [''];
}
