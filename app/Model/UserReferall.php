<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserReferall extends Model
{
    protected $table = 'user_referrals';
    protected $primarykey = 'id';
    protected $guarded = [''];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
