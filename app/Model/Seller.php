<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    protected $table = 'access_role';
    protected $primaryKey = 'id';
    protected $guarded = [''];

}
