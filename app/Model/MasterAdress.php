<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MasterAdress extends Model
{
    protected $table = 'master_address';
    protected $primaryKey = 'id';
    protected $guarded = [''];
    protected $appends = ['status_label'];

    public function getStatusLabelAttribute()
    {
        if ($this->is_default == 0) {
            return '<span class="badge badge-success">Mati</span>';
        } elseif ($this->is_default == 1) {
            return '<span class="badge badge-warning">Aktif</span>';
        }
    }

    public function countrys()
    {
        return $this->belongsTo('App\Model\Country', 'province');
    }

    public function citys()
    {
        return $this->belongsTo('App\Model\City', 'district');
    }

    public function states()
    {
        return $this->belongsTo('App\Model\State', 'city');
    }
}
