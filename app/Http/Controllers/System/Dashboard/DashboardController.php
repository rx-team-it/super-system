<?php

namespace App\Http\Controllers\System\Dashboard;

use App\Http\Controllers\Controller;
use App\Model\AccessRole;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function dashboard()
    {
        $orstat = User::select(DB::raw("COUNT(*) as count"))->whereYear('created_at', date('Y'))->groupBy(DB::raw("Month(created_at)"))->pluck('count');
        $months = User::select(DB::raw("Month(created_at) as month"))->whereYear('created_at', date('Y'))->groupBy(DB::raw("Month(created_at)"))->pluck('month');

        $datase = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        foreach ($months as $index => $month) {
            $datase[$month - 1] = $orstat[$index];
        }

        $vendor = AccessRole::where('role_id', '=', 1)->count();
        $seller = AccessRole::where('role_id', '=', 2)->count();
        $reseller = AccessRole::where('role_id', '=', 4)->count();
        $user = AccessRole::count();

        return view('system.dashboard.index', compact('datase', 'vendor', 'seller', 'reseller', 'user'));
    }
}
