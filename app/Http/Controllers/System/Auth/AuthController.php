<?php

namespace App\Http\Controllers\System\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function formLogin()
    {
        return view('system.auth.login');
    }

    public function postLogin(Request $request)
    {
        $CheckUser = User::where('email', $request->email)->first();

        if (empty($CheckUser)) {
            return redirect()->back()->with('error', 'Email Tidak Ditemukan');
        } else {
            if (Hash::check($request->password, $CheckUser->password)) {
                if (!Auth::attempt([
                    'email' => $request->email,
                    'password' => $request->password
                ])) {
                    return redirect()->back()->with('error', 'Silahkan Isi Dengan Benar');
                }
                return redirect()->route('dashboard')->with(['success' => 'Login Sukses']);
            } else {
                return redirect()->back()->with('error', 'Password anda salah.!');
            }
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('formLogin')->with(['success' => 'Logout Berhasil']);
    }
}
