<?php

namespace App\Http\Controllers\System\Vendor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Model\AccessRole;
use App\Model\MasterStore;
use App\Model\UserReferall;
use App\Model\Slider;
use App\Model\MasterAdress;
use App\Model\MasterVendor;
use Illuminate\Support\Facades\Validator;

class VendorController extends Controller
{
    public function index()
    {
        $user = AccessRole::where('role_id', '=', 1)->with('user', 'role')->paginate(6);

        return view('system.vendor.list_vendor', compact('user'));
    }

    public function register()
    {
        return view('system.vendor.register');
    }

    public function daftar(Request $request)
    {
        $rules = [
            'name' => 'required|string|between:4,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|confirmed|min:6',
            'phone' => 'required|numeric|min:9',
        ];

        $messages = [
            'name.between'           => 'Minimal 4 Karakter',
            'name.required'          => 'Nama wajib diisi.',
            'password.required'      => 'Password wajib diisi.',
            'password.min'           => 'Password minimal diisi dengan 5 karakter.',
            'password.confirmed'     => 'Password Tidak Sesuai',
            'email.required'         => 'Email wajib diisi.',
            'email.email'            => 'Email tidak valid.',
            'email.unique'           => 'Email sudah terdaftar.',
            'phone.numeric'          => 'Wajib Diisi Dengan Angka',
            'phone.required'         => 'Nomor Telp Wajib Diisi'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json(['status' => 0, 'error' => $validator->errors()->toArray()]);
        } else {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'password' => bcrypt($request->password)
            ]);

            $char = 'AB';

            $number = $char . '-' . rand(100, 1000);

            $vendor = MasterVendor::create([
                'user_id' => $user->id,
                'url_site' => $request->url_site,
                'domain' => $request->domain,
                'name_site' => $request->name_site,
                'vendor_code' => $number,
                'contact_phone' => $request->contact_phone,
                'contact_address' => $request->contact_address,
                'contact_email' => $request->contact_email,
                'description' => $request->description,
                'status' => 0,
                'meta_desc' => $request->meta_desc,
                'code_reff' => $char
            ]);

            AccessRole::create([
                'role_id' => 1, //vendor
                'user_id' => $user->id
            ]);

            $store = MasterStore::create([
                'user_id' => $user->id
            ]);

            $characters = 'AB';

            $pin = $characters . '-' . rand(100000, 1000000);

            UserReferall::create([
                'user_id' => $user->id,
                'referral_code' => $pin
            ]);

            MasterAdress::create([
                'id_user' => $user->id,
                'id_store' => $store->id
            ]);
        }

        return response()->json(['status' => 1, 'msg' => 'Vendor Berhasil Dibuat']);
    }
}
