<?php

namespace App\Http\Controllers\System\Aws;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\HistoryImage;
use DB;

class AwsController extends Controller
{
    public function index()
    {
        $r = HistoryImage::orderByDesc('created_at')->get();
        $w = DB::table('history_image')
            ->leftJoin('users', 'history_image.user_id', '=', 'users.id')
            ->orderByDesc('history_image.created_at')
            ->get([
                'history_image.gambar',
                'history_image.ukuran',
                'history_image.created_at',
                'users.name'
            ]);
        $no = 1;
        $e = DB::select("SELECT 
                sum(ukuran) as size
            FROM
                history_image")[0];

        return view('system.aws.index', compact('r', 'no', 'e', 'w'));
    }
}
