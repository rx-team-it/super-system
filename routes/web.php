<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'System\Auth\AuthController@formLogin')->name('formLogin');
Route::post('/login', 'System\Auth\AuthController@postLogin')->name('login');
Route::get('/logout', 'System\Auth\AuthController@logout')->name('logout');

Route::group([
    'middleware' => 'system'
], function () {
    Route::get('/dashboard', 'System\Dashboard\DashboardController@dashboard')->name('dashboard');

    Route::group([
        'prefix' => 'vendor',
    ], function () {
        Route::get('/list-vendor', 'System\Vendor\VendorController@index')->name('vendor.index');
        Route::get('/register-vendor', 'System\Vendor\VendorController@register')->name('vendor.register');
        Route::post('/regist-vendor', 'System\Vendor\VendorController@daftar')->name('vendor.post');
    });

    Route::group([
        'prefix' => 'aws-img',
    ], function () {
        Route::get('/index', 'System\Aws\AwsController@index')->name('aws.index');
    });
});
